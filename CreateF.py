from imutils import paths
import random

img_list1 = list(paths.list_images('F:/NeuralNetworks/imagesOriginal'))
img_list2 = list(paths.list_images('F:/NeuralNetworks/outimgFilter'))
list = []

i = 0
for imagePath in img_list1:
    or_dict = dict()
    or_dict.update({'name': img_list1[i], 'label': 0})
    list.append(or_dict)
    i += 1

j = 0
'''for imagePath in img_list2:
    or_dict = dict()
    or_dict.update({'name': img_list2[i], 'label': 1})
    list.append(or_dict)
    j += 1'''

q = 0
def CreateFile(filename, list):
    global q
    f = open(filename, 'w')
    p = 1
    '''while p <= s:
        f.write(list[q] + '\n')
        p += 1
        q += 1'''
    for l in list:
        f.write(l["name"] + ";" + str(l["label"]) + '\n')
    f.close()

random.shuffle(list)
k = len(list)
k_vt = int(k*0.1)
k_train = k - k_vt*2
CreateFile("Validation.csv", list[0:k_vt])
CreateFile("Test.csv", list[k_vt : 2*k_vt])
CreateFile("Train.csv", list[2*k_vt:])
#print(list)