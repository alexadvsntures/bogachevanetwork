# USAGE
# python align_faces.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg

# import the necessary packages
from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb
from imutils import paths
import cv2
import argparse
import imutils
import dlib


# construct the argument parser and parse the arguments
'''ap = argparse.ArgumentParser()
ap.add_argument("-p", "--shape-predictor", required=True,
	help="path to facial landmark predictor")
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
args = vars(ap.parse_args())'''

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor and the face aligner
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("C:/shape_predictor_68_face_landmarks.dat")
img_list = list(paths.list_images('F:/Neural Networks/images'))
fa = FaceAligner(predictor, desiredFaceWidth=256)
i = 0

for imagePath in img_list:
# load the input image, resize it, and convert it to grayscale
	print(img_list[i])
	i += 1
	image = cv2.imread(imagePath)
	image = imutils.resize(image, width=800)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

	# show the original input image and detect faces in the grayscale
	# image
	#cv2.imshow("Input", image)
	rects = detector(gray, 2)

	# loop over the face detections
	for rect in rects:
		# extract the ROI of the *original* face, then align the face
		# using facial landmarks
		(x, y, w, h) = rect_to_bb(rect)
		faceOrig = imutils.resize(image[y:y + h, x:x + w], width=256)
		faceAligned = fa.align(image, gray, rect)

		import uuid
		f = str(uuid.uuid4())
		cv2.imwrite("F:/Neural Networks/outimg/" + f + ".png", faceAligned)

		# display the output images
		'''cv2.imshow("Original", faceOrig)
		cv2.imshow("Aligned", faceAligned)
		cv2.waitKey(0)'''